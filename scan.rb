# Stefan Behr
# CSE 413
# Homework 7
# 11/29/2012

module Scanner
  class Token
    attr_reader :value, :tokstr

    def initialize(kind, tokstr="")
      # set the instance variables for the
      # kind of token and the token string
      # which gave rise to it
      @kind = kind
      @tokstr = tokstr
      if kind == :ID
        @value = tokstr
      elsif kind == :NUM
        @value = tokstr.to_f
      else
        @value = nil
      end
    end

=begin
The Token.kind method can return the following strings (as determined by the Scanner class):
  "EOL" (end of line), "ID" (identifier), "NUM" (floating point number),
  "LPAREN" (left parenthesis), "RPAREN" (right parenthesis), "ASSIGN" (assignment),
  "PLUS" (addition operator), "MINUS" (subtraction operator), "POWER" (exponentiation operator),
  "PROD" (multiplication operator), "QUOT" (division operator), "UNSET" (unset identifier keyword),
  "LIST" (list bound identifiers keyword), "QUIT" and "EXIT" (exit program), "SQRT" (builtin square
  root function keyword)
=end
    def kind
      @kind.to_s
    end

=begin
Token.to_s returns a string representation of a Token object. The representation
consists of a left angle bracket, the kind of Token, and a separator followed
by the value of the Token, only if the Token has a non-nil value (i.e., if it's
an identifier or number).
=end
    def to_s
      sep = value ? ":" : "" # only use separator if value is not nil
      "<#{kind}#{sep}#{value}>"
    end
  end

  class Scanner
    # list of builtin keywords
    @@keywords = ["unset", "list", "quit", "exit", "sqrt"]

    # deliberately ordered list of lexical classes, with
    # the lexical class name for the multiplication operator
    # following the name for the power operator, since the
    # latter is a substring of the former.
    @@lexical_classes = [
        :EOL, :ID, :NUM,
        :LPAREN, :RPAREN, :ASSIGN,
        :PLUS, :MINUS, :POWER,
        :PROD, :QUOT
    ]

    # regex patterns for capturing instances of
    # each lexical class (except keywords, since
    # those are handled as special cases of
    # identifiers)
    @@patterns = {
      :EOL  =>  /\n/,
      :ID   =>  /[a-zA-Z][a-zA-Z0-9_]*/,
      :NUM  =>  /(0|[1-9][0-9]*)(\.[0-9]+)?([eE][0-9]+)?/,
      :LPAREN =>  /\(/,
      :RPAREN =>  /\)/,
      :ASSIGN =>  /=/,
      :PLUS =>  /\+/,
      :MINUS  =>  /\-/,
      :PROD =>  /\*/,
      :QUOT =>  /\//,
      :POWER  =>  /\*\*/,
      :WHITESPACE  =>  /[ \t]+/
    }

    def initialize
      # initialize instance variable to empty string,
      # because string emptiness will be a test for
      # grabbing the next line of input
      @line = ""
    end

    def next_token
      if not @line.nil? and @line.empty? # previous line processed, or just starting scanning, haven't reached EOF
        @line = STDIN.gets
      end
      if @line.nil? # result of STDIN.gets is nil if end of input reached
        return Token.new(:EOF)
      else
        token = nil
        @line.sub!(/\A#{@@patterns[:WHITESPACE]}/, "")  # strip leading non-newline whitespace
        @@lexical_classes.each {|lexical_class|
          pattern = /\A#{@@patterns[lexical_class]}/
          match = pattern.match(@line)
          if not match.nil?  # positive match
            token_string = match[0]
            # check if what we captured is a reserved keyword
            # since those are a subset of IDs
            if @@keywords.include?(token_string)
              lexical_class = token_string.upcase.to_sym
            end
            token = Token.new(lexical_class, token_string)
            break
          end
        }
        if token.nil?
          # this means no valid token was found, so we'll advance by one char
          bad_char = @line.slice!(0).chr  # delete first char from line
          #puts "Warning: no valid token found; ignoring '#{bad_char}' character"
          token = self.next_token
        end
        n = token.tokstr.length # length of found token
        @line.slice!(Range.new(0, n, exclusive=true)) # chop found token off of input line (in place)
        return token
      end
    end
  end

  # run on test input until seeing
  # quit, exit, or end of file
  scanner = Scanner.new
  exits = ["<QUIT>", "<EXIT>", "<EOF>"] # exit if we see one of these
  begin
    token = scanner.next_token
    if not token.nil? # print only valid tokens
      puts token
    end
  end while not exits.include?(token.to_s)
end