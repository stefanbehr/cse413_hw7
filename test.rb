# Stefan Behr
# CSE 413
# Homework 7
# 11/29/2012

test1 = "a = 4*5/(900**50)-4e10+5.0E100 list unset"
test2 = "5"
test3 = "[]       \t\t \n  56*7 - 90 \t 19*7**8"

tests = [test1, test2, test3]

tests.each {|test|
  puts "Test string: #{test.inspect}"
  IO.popen("ruby scan.rb", "w") {|io|
    io.write(test)
  }
  puts "** end of test**"
}